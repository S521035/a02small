 var App = {
    launch: function() {
        level = $("#level").val();
        attack = $("#attack").val();
        defense = $("#defense").val();
        move = $("#move").val();
        effective = $(".effective:checked").val();
        
        if (($("#dual:checked").val()) == "2" && (effective == ".5")){
            effective = ".25";
        }
        else if (($("#dual:checked").val() == "2") && (effective == "2")){
            effective = "4";
        }

        dual = "1";
        stab = "1";
        
        if ($("#stab:checked").val() == "1.5")
        {
            stab = "1.5";
        }

        greenLight = App.checkValues(level, attack, defense, move);
        console.log(greenLight);
        if(greenLight === true)
        {
        App.calculation();
        }
    },

    calculation: function() { 
        var random = (Math.random() * 16) + 85;
        var outcome = Math.round(((((2 * parseInt(level) / 5 + 2) * parseInt(attack) * parseInt(move) / parseInt(defense) / 50) + 2) * parseFloat(stab) *
        parseFloat(effective) * parseInt(random)/ 100));
        console.log(outcome);
        $("#outcome").html(outcome);
        $(".text").html(" damage done");
        var critical = Math.round(outcome * 1.5);
        console.log(critical);
        $("#critical").html(critical);
        $(".text2").html(" damage with a critical hit");
    },

    checkValues: function(lvl, atk, def, mv){
            if (lvl < 1){
                alert("The given level is below the minimum.");
                return false;
            }
            else if (lvl > 100){
                alert("The given level is above the maximum.");
                return false;
            }
            else if(atk < 10){
                alert("The value for attack is below the minimum");
                return false;
            }
            else if(atk > 300){
                alert("The value for attack is above the maximum");
                return false;
            }
            else if(def < 10){
                alert("The value for defense is below the minimum");
                return false;
            }
            else if(def > 300){
                alert("The value for defense is above the maximum");
                return false;
            }
            else if(mv < 20){
                alert("The value for the move's power is below the minimum");
                return false;
            }
            else if(mv > 175){
                alert("The value for move's power is above the maximum");
                return false;
            }
            else {
                return true;
            }
        }
};