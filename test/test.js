// This file tests our application code - open this file in a browser to see the test results. 

// It uses QUnit, a unit testing library for JavaScript. 

// Unit testing is a common, professional practice. 

// It helps us verify our code is correct in all cases.

QUnit.test("With all parameters acceptable, should return true", function (assert) {
    var result = App.checkValues(50, 150, 150, 95);
    assert.equal(result, true, "result was " + result);
});

QUnit.test("Level set as 0, should return result of false", function (assert) {
    var result = App.checkValues(0, 150, 150, 95);
    assert.equal(result, false, "result was " + result);
});

QUnit.test("Level set to 150, should return false", function (assert) {
    var result = App.checkValues(150, 150, 150, 95);
    assert.equal(result, false, "result was " + result);
});

QUnit.test("Attack set to 0, should return false", function (assert) {
    var result = App.checkValues(50, 0, 150, 95);
    assert.equal(result, false, "result was " + result);
});
QUnit.test("Attack set to 450, should return false", function (assert) {
    var result = App.checkValues(50, 450, 150, 95);
    assert.equal(result, false, "result was " + result);

});QUnit.test("Defense set to 0, should return false", function (assert) {
    var result = App.checkValues(50, 150, 0, 95);
    assert.equal(result, false, "result was " + result);

});QUnit.test("Defense set to 450, should return false", function (assert) {
    var result = App.checkValues(50, 150, 450, 95);
    assert.equal(result, false, "result was " + result);
});

QUnit.test("Move set to 0, should return false", function (assert) {
    var result = App.checkValues(50, 150, 150, 0);
    assert.equal(result, false, "result was " + result);
});

QUnit.test("Move set to 250, should return false", function (assert) {
    var result = App.checkValues(50, 150, 150, 250);
    assert.equal(result, false, "result was " + result);
});





